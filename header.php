<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CityMunch_2018
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php // Additional classes

$headerClasses = '';
if(is_front_page()): 
	$headerClasses = ' is-front';
else:
	$headerClasses = ' not-front';
endif;

?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'citymunch_2018' ); ?></a>

	<header id="main-header" class="site-header <?php echo $headerClasses; ?> is-top menu-closed">
		<div class="animating-bg">
			<div class="circle"></div>
		</div>
		<div class="wrapper-big">
		
			<nav id="main-nav" class="site-nav grid-x">
			
				<div class="cell medium-shrink auto logo-container">
					<h1 class="logo">CityMunch</h1>
				</div>
				<div class="cell shrink mobile-menu-button-container show-for-small-only">
					<button class="hamburger-button" id="mm-button">
						<div class="bar bar-1"></div>
						<div class="bar bar-2"></div>
						<div class="bar bar-3"></div>
					</button>
				</div>
				<div class="cell medium-auto menu-container">

					<?php wp_nav_menu( array(
							'theme_location'=>'header-menu-1',
							'menu'		=>	'header-menu-1',
							'menu_class'=>	'main-header-menu',
							'fallback_cb'=>	false,
							'depth'		=>	2,
							'item_spacing'=>'discard'
						) ); ?>

				</div>

			</nav>

		</div>
	</header><!-- #masthead -->
	<div class="menu-opened-overlay menu-closed" id="black-overlay"></div>

	<div id="content" class="site-content">

		<div class="spacer"></div>
