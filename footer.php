<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CityMunch_2018
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
		<nav class="footer-nav">
			<div class="wrapper-big">
				<div class="grid-x no-pad">
					<div class="social-menu cell small-12 medium-shrink">
						<ul class="social-icons-menu">
							<li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a></li>
							<li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a></li>
						</ul>
					</div>
					<div class="footer-menu cell small-12 medium-auto">
						<?php wp_nav_menu( array(
							'theme_location'=>'footer-menu-1',
							'menu'		=>	'footer-menu-1',
							'menu_class'=>	'main-footer-menu',
							'fallback_cb'=>	false,
							'depth'		=>	1,
							'item_spacing'=>'discard'
						) ); ?>
					</div>
					<div class="colophon cell small-12 medium-shrink">
						<p>
							Copyright &copy; <?php echo date('Y'); ?> CityMunch<br/>
							All Rights Reserved
						</p>
					</div>
				</div>
			</div>
		</nav>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
