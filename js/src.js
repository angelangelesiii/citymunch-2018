
(function($) {})( jQuery ); // JQuery WordPress workaround

jQuery(document).ready(function($){ // Document Ready

    // ScrollMagic init controller
    var controller = new ScrollMagic.Controller();
    var is_top = true;

    // navbar animation
    var navMenuTransition = new ScrollMagic.Scene({
        triggerElement: '#top',
        offset: '50px',
        triggerHook: 0,
    })
    .on('enter', function() {
        $('#main-header').addClass('not-on-top');
        $('#main-header').removeClass('is-top');
        is_top = false;
        // $('#siteheader').addClass('opaque');
    })
    .on('leave', function() {
        $('#main-header').removeClass('not-on-top');
        $('#main-header').addClass('is-top');
        is_top = true;
        // $('#siteheader').removeClass('opaque');
	})
    // .addIndicators()
    .addTo(controller);



    // Slick for partner restaurants
    $('.partners-container').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        arrows: false,
        infinite: true,
        variableWidth: true,
        centerMode: true,
        slidesToShow: 3,
        speed: 1000,
    });

    // Slick for steps
    $('.medium-large-version .phone-demo-container').slick({
        slidesToShow: 1,
        dots: true,
        arrows: false,
        infinite: false,
        autoplay: false,
    })

    $('.medium-large-version .step-text.step-1 .step-number').on('click', function(){
        $('.medium-large-version .phone-demo-container').slick('slickGoTo', 0);
    })
    $('.medium-large-version .step-text.step-2 .step-number').on('click', function(){
        $('.medium-large-version .phone-demo-container').slick('slickGoTo', 1);
    })
    $('.medium-large-version .step-text.step-3 .step-number').on('click', function(){
        $('.medium-large-version .phone-demo-container').slick('slickGoTo', 2);
    })


    // CounterUp for Numbers section
    var numbersStagger = new TimelineMax()
        .staggerFrom('.numbers .item h3', 0.5, {
            scale: 0,
            ease: Back.easeOut,
        }, 0.2);

    var numbersCount = new ScrollMagic.Scene({
        triggerElement: '.numbers',
        reverse: false,
    })
    .setTween(numbersStagger)
    .on('enter', function(e) {
        // $('.numbers .item h3').show();
    })
    // .addIndicators()
    .addTo(controller);


    // Mobile menu animation
    var dropLength = $('#main-header .menu-container').outerHeight() + $('#main-header').outerHeight();
    console.log(dropLength);

    $(window).resize(function() {
        dropLength = $('#main-header .menu-container').outerHeight() + $('#main-header').outerHeight();
        console.log(dropLength);
    });

    var mmAnimate = new TimelineMax()
        // sequence 1
        .to('#mm-button .bar-1', 0.15, {
            y: 14,
            ease: Power3.easeIn
        }, 'a')
        .to('#mm-button .bar-3', 0.15, {
            y: -14,
            ease: Power3.easeIn
        }, 'a')
        .to('#mm-button .bar-2', 0.15, {
            width: 0,
            ease: Power3.easeIn
        }, 'a')
        // sequence 2
        .to('#mm-button .bar-1', 0.15, {
            rotation: 45,
            width: '110%',
            ease: Power3.easeOut
        }, 'b')
        .to('#mm-button .bar-3', 0.15, {
            rotation: -45,
            width: '110%',
            ease: Power3.easeOut
        }, 'b')
        .pause(); // stop timelinemax

    var mmPaneAnimate = new TimelineMax()
        .to('#main-header .menu-container', 0.4, {
            y: dropLength - 1,
            ease: Power4.easeOut
        }, 'a')
        .pause();

    function openMenu() {
        mmAnimate.play(0);
        mmPaneAnimate.play(0);
        $('#main-header').removeClass('menu-closed');
        $('#main-header').addClass('menu-opened');
        $('#black-overlay').removeClass('menu-closed');
        $('#black-overlay').addClass('menu-opened');
        $('body').addClass('menu-opened');
    }

    function closeMenu() {
        mmAnimate.reverse(0);
        mmPaneAnimate.reverse(0);
        $('#main-header').removeClass('menu-opened');
        $('#main-header').addClass('menu-closed');
        $('#black-overlay').removeClass('menu-opened');
        $('#black-overlay').addClass('menu-closed');
        $('body').removeClass('menu-opened');
    }

    $('#mm-button').on('click', function(e) {
        if($('#main-header').hasClass('menu-closed')) {
            openMenu();            
        } else {
            closeMenu();
        }
    })

    $('#black-overlay').on('click', function(e) {
        closeMenu();
    });
        
});