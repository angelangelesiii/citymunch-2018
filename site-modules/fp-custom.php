<?php 
/*
*
*   This serves as the custom front page skeleton. Edit as you please.
*   For help, please contact the original developer at angelanglesiii@gmail.com
*
*/
?>

<div class="front-page custom-section">
    
    <!-- HERO BANNER -->
    <section class="hero-banner">
        <div class="wrapper">

            <div class="grid-x">
                <div class="cell small-12 medium-5 banner-image">
                    <img src="<?php echo get_template_directory_uri().'/images/home/iPhoneX_CM_banner.png'; ?>" alt="">
                </div>
                <div class="cell small-12 medium-7 banner-contents">
                    <div class="inner">
                        <h1 class="banner-heading">
                            Find great deals on food in your area...
                        </h1>
                        <p class="banner-subtext">
                            Get money off and discover new foodie destinations.
                        </p>

                        <form action="#" method="get" class="hero-search grid-x">
                            <input type="search" name="cm_search" id="cm-search" class="cell auto" placeholder="Enter your postcode or city...">
                            <button type="submit" class="cell shrink">Find food</button>
                        </form>

                        <ul class="mobile-stores">
                            <li><a href="#"><img src="<?php echo get_template_directory_uri().'/images/appstore.png'; ?>" alt="" class="store"></a></li>
                            <li><a href="#"><img src="<?php echo get_template_directory_uri().'/images/googleplay.png'; ?>" alt="" class="store"></a></li>
                        </ul>
                        
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- PARTNERS -->
    <section class="restaurant-partners">

        <?php 
            $baseDir = get_template_directory_uri().'/images/affiliates/';
            $partner = array(
                $baseDir.'busaba.png',
                $baseDir.'coqfighter.png',
                $baseDir.'HOP.png',
                $baseDir.'good_yard.jpeg',
                $baseDir.'k10.png',
                $baseDir.'tapas_brindisa.png',
                $baseDir.'tossed-logo.png',
                $baseDir.'vital-ingredient-logo.png',
            );
        ?>

        <div class="wrapper-medium">
            <h2 class="section-heading">
                The CityMunch Community
            </h2>
            <p class="subtext">
            We’re constantly adding new foodie spots, stay tuned!
            </p>

            <div class="partners-slider">
                <div class="partners-container">
    
                    <?php foreach($partner as $restaurant): ?>
    
                    <div class="partner-item">
                        <img src="<?php echo $restaurant; ?>" alt="" class="partner-logo">
                    </div>
    
                    <?php endforeach; ?>
    
                </div>
            </div>
        </div>
    </section>

    <!-- STEPS -->
    <section class="step-by-step">

        <div class="medium-large-version">
            <div class="wrapper-big">
                <div class="grid-x">
                    
                    <div class="cell medium-4 phone-demo-container large hide-for-small-only">
                        <div class="step-image">
                            <img src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                        </div>
                        <div class="step-image">
                            <img src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                        </div>
                        <div class="step-image">
                            <img src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                        </div>
                    </div>
    
                    <div class="cell medium-8 small-12 auto steps-container">
                        <div class="grid-x inner">
                            <div class="cell large-4 step-text step-1">
                                <img src="<?php echo get_template_directory_uri().'/images/steps/1.png' ?>" alt="" class="step-number">
                                <h3 class="step-label">Find a deal</h3>
                                <p class="detail">Pick what you fancy eating from what restaurants are nearby.</p>
                                <img class="demo show-for-small-only" src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                            </div>
                            <div class="cell large-4 step-text step-2">
                                <img src="<?php echo get_template_directory_uri().'/images/steps/2.png' ?>" alt="" class="step-number">
                                <h3 class="step-label">Get a voucher</h3>
                                <p class="detail">For just yourself or friends too, from 20-100% off!</p>
                                <img class="demo show-for-small-only" src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                            </div>
                            <div class="cell large-4 step-text step-3">
                                <img src="<?php echo get_template_directory_uri().'/images/steps/3.png' ?>" alt="" class="step-number">
                                <h3 class="step-label">Get munching</h3>
                                <p class="detail">You order and pay when you get to the restaurant, simple!</p>
                                <img class="demo show-for-small-only" src="<?php echo get_template_directory_uri().'/images/home/ip_1.png' ?>" alt="">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    
    </section>

    <!-- MAP -->
    <section class="cm-map">
        <div class="shadow"></div>
        
        <div class="grid-x">
            <div class="map-section cell medium-8 large-8">

            </div>
            <div class="text-section cell medium-4 large-4">
                <div class="inner">
                    <h2>
                        <span class="big">100s</span>
                        of restaurants across London
                    </h2>
                    <p class="hide-for-small-only">
                    From Poke bowls to tapas, slow food to speedy lunches, we’ve got it covered
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- GET STARTED -->
    <section class="get-started">
        <div class="wrapper-medium">
            <div class="text-content">
                <h2>Get started today</h2>
                <p>Find eats and save money with CityMunch</p>
                <ul class="mobile-stores">
                    <li><a href="#"><img src="<?php echo get_template_directory_uri().'/images/appstore.png'; ?>" alt="" class="store"></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri().'/images/googleplay.png'; ?>" alt="" class="store"></a></li>
                </ul>
            </div>
        </div>
        <img src="<?php echo get_template_directory_uri().'/images/getting-started.png'; ?>" alt="" class="burger-bg">
    </section>

    <!-- TESTIMONIALS -->
    <section class="testimonials">
        <div class="wrapper-medium">
            <h2 class="section-title">What our users say</h2>
            
            <div class="testimonials-container grid-x">
                <div class="testimonial cell medium-4 small-12">
                    <div class="stars">
                        <ul>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                        </ul>
                    </div>
                    <div class="customer-dialog">
                        <p>Munch 'n' save. A great way to discover new local restaurants and save money at the same time!</p>
                    </div>
                    <div class="customer-photo"></div>
                    <div class="customer-name">Shazia</div>
                </div>

                <div class="testimonial cell medium-4 small-12">
                    <div class="stars">
                        <ul>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                        </ul>
                    </div>
                    <div class="customer-dialog">
                        <p>My favourite place would NEVER have been discovered were it not for your discount. I work in the city but I'm not a banker, so your discounts make food affordable for me.</p>
                    </div>
                    <div class="customer-photo"></div>
                    <div class="customer-name">Lisa</div>
                </div>

                <div class="testimonial cell medium-4 small-12">
                    <div class="stars">
                        <ul>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                            <li><i class="fas fa-star"></i></li>
                        </ul>
                    </div>
                    <div class="customer-dialog">
                        <p>Incredibly useful when I don't know what I want. Only getting better as they add more restaurants!</p>
                    </div>
                    <div class="customer-photo"></div>
                    <div class="customer-name">Arik</div>
                </div>
            </div>
        </div>
    </section>

    <!-- NUMBERS -->
    <section class="numbers">
        <div class="wrapper-medium">
            <div class="numbers-container grid-x">
                <div class="item cell medium-4 small-12">
                    <div class="banner-container">
                        <div class="bg-circle"></div>
                        <h3>
                            <span class="main"><span class="number">270</span>+</span>
                            <span class="small">restaurants</span>
                        </h3>
                    </div>
                    <p class="subtext">...and growing</p>
                </div>
    
                <div class="item cell medium-4 small-12">
                    <div class="banner-container">
                        <div class="bg-circle"></div>
                        <h3>
                            <span class="small">over</span>
                            <span class="main"><span class="number">80,000</span></span>
                            <span class="small">loyal users</span>
                        </h3>
                    </div>
                    <p class="subtext">​that rely on us to keep the menus in front of them fresh</p>
                </div>
    
                <div class="item cell medium-4 small-12">
                    <div class="banner-container">
                        <div class="bg-circle"></div>
                        <h3>
                            <span class="small">over</span>
                            <span class="main">£<span class="number">330,000</span></span>
                            <span class="small">saved</span>
                        </h3>
                    </div>
                    <p class="subtext">We have helped loads of people just like you save on eating out</p>
    
                </div>
            </div>
        </div>
    </section>

    <!-- QUOTE -->
    <section class="quote">
        <div class="wrapper">
            <div class="inner">
                <img src="<?php echo get_template_directory_uri().'/images/qm.png' ?>" alt="" class="quotation-marks">
                <div class="text">
                    <blockquote class="main">The app that's changing the way we use restaurants</blockquote>
                    <cite>Telegraph</cite>
                </div>
            </div>
        </div>
    </section>

    <!-- WHO'S TALKING -->
    <section class="talk">
        <div class="wrapper-medium">
            <h2>Look who's talking!</h2>
            <div class="grid-x media-container">
                <div class="media cell small-6 medium-shrink">
                   <img src="<?php echo get_template_directory_uri().'/images/media/London_Evening_Standard.png'; ?>" alt="">     
                </div>
                <div class="media cell small-6 medium-shrink">
                    <img src="<?php echo get_template_directory_uri().'/images/media/The_Tab.png'; ?>" alt=""> 
                </div>
                <div class="media cell small-6 medium-auto">
                    <img src="<?php echo get_template_directory_uri().'/images/media/The_Daily_Telegraph.png'; ?>" alt=""> 
                </div>
                <div class="media cell small-6 medium-shrink">
                    <img src="<?php echo get_template_directory_uri().'/images/media/B247.png'; ?>" alt=""> 
                </div>
            </div>
        </div>
    </section>

</div>

